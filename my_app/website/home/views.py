from django.shortcuts import render,HttpResponse
from .models import Photo
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
# Create your views here.
def home(request):
    content={
        'photos':Photo.objects.all()
    }
    return render(request,'index.html',content)

class PhotoUpdate(UpdateView):
    model=Photo
    fields=['title','width','height','image']
class PhotoDelete(DeleteView ):
    model=Photo
    success_url=reverse_lazy('index.html')
